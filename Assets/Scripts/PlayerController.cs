using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private int estado = 0;
    private float velocidad = 10f;

    public GameObject BalaPrefrab1;
    public GameObject BalaPrefrab2;
    public GameObject BalaPrefrab3;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        estado = 0;
        animator.SetInteger("Estado", estado);
        rb.velocity = new Vector2(0, rb.velocity.y);
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            estado = 2;
            animator.SetInteger("Estado", estado);
            rb.velocity = new Vector2(velocidad, 45);
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKeyDown(KeyCode.X))
        {
            estado = 3;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
            if (Time.time <= 1 && Time.time < 3)
            {
                Disparar(BalaPrefrab1);
            }
            if (Time.time >= 3 && Time.time < 5)
            {
                Disparar(BalaPrefrab2);
            }
            if(Time.time >= 5)
            {
                Disparar(BalaPrefrab3);
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.X))
        {
            estado = 3;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
            if (Time.time <= 1 && Time.time < 3)
            {
                Disparar(BalaPrefrab1);
            }
            if (Time.time >= 3 && Time.time < 5)
            {
                Disparar(BalaPrefrab2);
            }
            if (Time.time >= 5)
            {
                Disparar(BalaPrefrab3);
            }
        }

    }

    private void Disparar(GameObject balas)
    {
        var x = this.transform.position.x;
        var y = this.transform.position.y;

        var bala = Instantiate(balas, new Vector2(x, y), Quaternion.identity);

        if (sprite.flipX)
        {
            var controller = balas.GetComponent<BalaController>();
            var flip = bala.GetComponent<SpriteRenderer>();
            controller.velocidad = controller.velocidad * -1;
            flip.flipX = true;
        }
    }

}
