using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private float velocidad = 6f * -1;
    private int vida = 5;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);
        sprite.flipX = true;
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bala_1")
        {
            if (vida <= 0)
            {
                animator.SetInteger("Estado", 1);
                Destroy(this.gameObject);
            }
            else
            {
                vida = vida - 1;
            }
        }
        if (collision.gameObject.tag == "Bala_2")
        {
            if (vida <= 0)
            {
                animator.SetInteger("Estado", 1);
                Destroy(this.gameObject);
            }
            else
            {
                vida = vida - 3;
            }
        }
        if (collision.gameObject.tag == "Bala_3")
        {
            if (vida <= 0)
            {
                animator.SetInteger("Estado", 1);
                Destroy(this.gameObject);
            }
            else
            {
                vida = vida - 5;
            }
        }
    }
}
